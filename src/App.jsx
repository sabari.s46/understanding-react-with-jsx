// import React from 'react';
// import ReactDOM from 'react-dom/client';
import "./App.css";
import elephant from "./images/elephant.jpeg";

const imageData = () => {
  let data = [
    {
      id: 1,
      img: elephant,
    },
    {
      id: 2,
      img: elephant,
    },
    {
      id: 3,
      img: elephant,
    },
    {
      id: 4,
      img: elephant,
    },
  ];
  return data;
};

function App() {
  // code here
  return (
    <div>
      <div className="title">Kalvium Gallary</div>
      <div className="grid">
        <div>
          {" "}
          <img src={elephant} alt="" />
        </div>
        <div>
          {" "}
          <img src={elephant} alt="" />
        </div>
        <div>
          {" "}
          <img src={elephant} alt="" />
        </div>
        <div>
          {" "}
          <img src={elephant} alt="" />
        </div>
      </div>
    </div>
  );
}

export default App;
